
# Laravel client for OpenSubtitles API

## Setup

Add your credentials to your .env file

```env
OPENSUBTITLES_USERNAME=
OPENSUBTITLES_PASSWORD=
OPENSUBTITLES_DEFAULT_LANGUAGE=
OPENSUBTITLES_USERAGENT=
```

## Usage

```php
$client = new NoMercy\OpenSubtitles\App\Client();
$response = $client->searchSubtitles([
    'sublanguageid' => 'dut',
    'moviehash' => '163ce22b6261f50a',
    'moviebytesize' => '2094235131',
])->toArray();
```

```php
$client = new NoMercy\OpenSubtitles\App\Client();
$response = $client->searchSubtitles([[
    'sublanguageid' => 'dut',
    'query' => 'Young Sheldon',
    'season' => '1',
    'episode' => '1'
]])->toArray();
```
