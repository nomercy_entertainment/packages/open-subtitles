<?php

namespace NoMercy\OpenSubtitles;

use Illuminate\Support\ServiceProvider;

class OpenSubtitlesServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
    }
    public function register()
    {
        //
    }
}
