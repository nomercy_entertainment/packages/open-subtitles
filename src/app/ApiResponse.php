<?php

declare(strict_types=1);

namespace NoMercy\OpenSubtitles\App;

class ApiResponse
{

    protected $response;
    protected $method;

    public function __construct(string $method, $response)
    {
        $this->method = $method;
        $this->response = $response;
    }

    public function toArray(): array
    {
        return collect($this->response)->toArray();
    }

    public function toJson(): array
    {
        return collect($this->response)->toJson();
    }
}
