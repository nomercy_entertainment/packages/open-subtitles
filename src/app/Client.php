<?php

declare(strict_types=1);

namespace NoMercy\OpenSubtitles\App;

use ErrorException;
use Exception;

class Client
{
    protected $endpoint = 'http://api.opensubtitles.org/xml-rpc';
    protected $useragent = 'OSTestUserAgent';
    protected $language = 'en';
    protected $username;
    protected $password;
    protected $token;

    public function __construct()
    {
        $this->username = env('OPENSUBTITLES_USERNAME');
        $this->password = env('OPENSUBTITLES_PASSWORD');
        $this->language = env('OPENSUBTITLES_DEFAULT_LANGUAGE', 'en');
        $this->useragent = env('OPENSUBTITLES_USERAGENT', 'TemporaryUserAgent');

        if (!function_exists('xmlrpc_encode_request')) {
            throw new ClientException(ClientException::ERR_MISSING_EXTENSION);
        }
        if (null === $this->username || null === $this->password) {
            throw new ClientException(ClientException::ERR_MISSING_USERNAME_PASSWORD);
        }
    }

    public function __destruct()
    {
        if (null !== $this->token) {
            $this->logOut($this->token);
        }
    }

    public function obtainToken(): string
    {
        if (null !== $this->token) {
            return $this->token;
        }
        $response = $this->logIn(
            $this->username,
            $this->password,
            $this->language,
            $this->useragent
        )->toArray();
        $this->token = $response['token'];

        return $this->token;
    }

    public function buildRequest(string $method, array $params = []): string
    {
        $request = xmlrpc_encode_request($method, $params, [
            'encoding' => 'UTF-8',
        ]);

        return $request;
    }

    public function get_FileContents($path, $use_include_path = null, $context = null)
    {
        try {
            return file_get_contents($path, $use_include_path, $context);
        } catch (ErrorException $exception) {
            dump($exception);
        } catch (Exception $exception) {
            dump($exception, false);
        }
    }

    public function sendRequest(string $request): array
    {
        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => 'Content-Type: text/xml',
                'content' => $request,
            ],
        ]);

        $file = $this->get_FileContents($this->endpoint, false, $context);
        $response = xmlrpc_decode($file, 'UTF-8');

        if (is_array($response) && xmlrpc_is_fault($response)) {
            throw new ClientException($response['faultString'], $response['faultCode']);
        }
        if ('503 Backend fetch failed' == $response['status']) {
            throw new ClientException(ClientException::ERR_SITE_INDERGOING_MAINTANANCE);
        }
        if ('503 Service Not Available' == $response['status']) {
            throw new ClientException(ClientException::ERR_SITE_INDERGOING_MAINTANANCE);
        }
        else if (empty($response['status']) || '200 OK' !== $response['status']) {
            dd($response['status']);
            throw new ClientException(ClientException::ERR_INVALID_RESPONSE_STATUS);
        }

        return $response;
    }

    public function __call(string $method, array $params = []): ApiResponse
    {
        $method = ucfirst($method);
        if (!in_array($method, [
            'ServerInfo',
            'LogIn',
            'LogOut',
        ], true)) {
            $token = $this->obtainToken();
            array_unshift($params, $token);
        }
        $request = $this->buildRequest($method, $params);
        $response = $this->sendRequest($request);

        return new ApiResponse($method, $response);
    }
}
