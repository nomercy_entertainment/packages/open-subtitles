<?php

declare(strict_types=1);

namespace NoMercy\OpenSubtitles\App;

use Exception;

class ClientException extends Exception
{
    const ERR_MISSING_EXTENSION = 'Missing xmlrpc extension';
    const ERR_MISSING_USERNAME_PASSWORD = 'Missing username or password';
    const ERR_INVALID_RESPONSE_STATUS = 'Invalid response status';
    const ERR_SITE_INDERGOING_MAINTANANCE = 'Opensubtitles.org is undergoing maintanance';
}
